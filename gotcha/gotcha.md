# Gotchas remote controlling Rigol DS1000E/D oscilloscopes.


## `:WAV:DATA?` can easily crash the DS1000E/D

The biggest quirk of the DS1000E/D: When the command `:WAV:DATA?` is issued
too quickly after another command that changed some setting or after `:RUN`
or `:AUTO`, the DS1000E/D simply crashes. Workarounds:
- Ensure that `:WAV:DATA?` is issued with a delay of at least 0.1 seconds
  after the last parameter change, or after the command `:RUN` has been sent
  to the device.
- Avoid querying `:WAV:DATA` at all while the DS1000 is executing the
  command `:AUTO`. Problem/challenge: To figure out if/when the DS1000
  found "good" settings for the timebase scale, vertical scale and other
  settings.

(This problem is not documented by Rigol.)


## No `*OPC?` command

Probably the second biggest quirk of the DS1000E/D: The SCPI command `*OPC?*`
(operation completed) is not implemented. This makes it hard to figure out
when for example the change of a setting is applied by the device,
or when an operation like `:AUTO?` is finally finished.

The workaround used in this library for setting changes: After changing
a parameter, read its current value until the read value equals the
new setting.

Workaround used by Rigol's Ultrascope: Add a delay of 0.12 seconds between
sending any SCPI commands.

(This workaround is not documented by Rigol. It was found by tracing the
communication between Ultrascope and a DS1052E with Wireshark.)


## Different representation of values when setting and reading a parameter

- Video trigger mode:

  Reading the video trigger mode can retun the value `EVEN FIELD`,
  `ODD FIELD` and `ALL LINES`. But in order to change the trigger mode,
  the values `EVEN`, `ODD`, `ALL` must be used:

      > :TRIG:VIDEO:MODE?
      EVEN FIELD
      > :TRIG:VIDEO:MODE ODD FIELD
      > :TRIG:VIDEO:MODE?
      EVEN FIELD
      > :TRIG:VIDEO:MODE ODD
      > :TRIG:VIDEO:MODE?
      ODD FIELD
      > :TRIG:VIDEO:MODE EVEN FIELD
      > :TRIG:VIDEO:MODE?
      ODD FIELD
      > :TRIG:VIDEO:MODE EVEN
      > :TRIG:VIDEO:MODE?
      EVEN FIELD
      > :TRIG:VIDEO:MODE ALL LINES
      > :TRIG:VIDEO:MODE?
      EVEN FIELD
      > :TRIG:VIDEO:MODE ALL
      > :TRIG:VIDEO:MODE?
      ALL LINES

  (Documented by Rigol.)

- The DS1000 returns `Coarse` or `Fine` for :CHAN1:VERN? but to change
  the values, `ON` or `OFF` must be used.

      > :CHAN1:VERN?
      Coarse
      > :CHAN1:VERN Fine
      > :CHAN1:VERN?
      Coarse
      > :CHAN1:VERN ON
      > :CHAN1:VERN?
      Fine
      > :CHAN1:VERN Coarse
      > :CHAN1:VERN?
      Fine
      > :CHAN1:VERN OFF
      > :CHAN1:VERN?
      Coarse

  (Documented by Rigol.)

- Most boolean like settings are represented by the strings `ON` and `OFF`,
  used for changing a setting as well as reading a setting. An exception is
  `CHANi:DISP`, for which the DS1000E/D returns `0` or `1`. Changing the
  setting is possible with `ON`, `OFF`, `1`, `0`.

      > :CHAN1:DISP?
      1
      > :CHAN1:DISP 0
      > :CHAN1:DISP?
      0
      > :CHAN1:DISP ON
      > :CHAN1:DISP?
      1
      > :CHAN1:DISP OFF
      > :CHAN1:DISP?
      0
      > :CHAN1:DISP ON
      > :CHAN1:DISP?
      1

- Floating point numbers are returned by the DS1000E/D in scientific notation.
  But when a parameter is set, scientific notation cannot be used.

    > :TIM:SCAL?
    2.000e-04
    > :TIM:SCAL 1.000e-04
    > :TIM:SCAL?
    2.000e-04
    > :TIM:SCAL 0.0001
    > :TIM:SCAL?
    1.000e-04

  (Not documented by Rigol.)

These examples of SCPI commands sent to a DS1000E/D and its responses were
produced with the following script:

    device = usbtmc.Instrument(0x1ab1, 0x0588)
    device.open()
    try:
        while True:
            text = input('> ')
            text = text.strip()
            if not text:
                continue
            if text[0] in (':', '*'):
                if '?' in text:
                    try:
                        print(device.ask(text))
                    except usb.core.USBTimeoutError:
                        print('(timeout)')
                else:
                    device.write(text)
            if text.lower().startswith('q'):
                break
    finally:
        device.write(':key:force')
        device.close()

## Measuerment values

The DS1000E/D returns measurement values as ASCII strings that represent
the measurement value as a floating point number in scientific notation.
Not documented in the DS100E/D Programming Guide is that the number is
sometimes prefixed with `<`  or `>`. This leads to unexpected runtime
errors on attempts to convert a string like '<1.0e-05' to a floating point
number.

On the other hand, this is a nice feature: The DS1000E/D seems to add
a '<' or '>' when it cannot make a sufficiently accurate measurement.

Example: When a 1 kHz square wave is connected to an input and when the
timebase scale is set to 100µs/div, the DS1000E/D shows the value
"<6.00e-6" for the rise time. This makes sense: With this timebase setting,
the DS1000E/D uses the sample rate 2.5e6/s, or one sample every 1/2.5e6
seconds -> 0.4µs. So the rise time is shorter than 6/0.4 -> 15 "sample
cycles". This looks reasonable: 15 samples are indeed not very much for
a time measurement.

## Partly wrong documentation of the conversion of the raw waveform data into volts and seconds.

The DS1000E/D sends a simple sequence of bytes as a response to the query
`:WAV:DATA?`. This is fine since it requires the minimum number of bytes
to transfer, and the DS1000E/D is a USB1.1 device, hence the transfer speed
is quite low.

Rigol's DS1000E/D Programming Guide does not document how to convert the raw
waveform data into volts and seconds at all. But they provide at least an
[FAQ page](https://rigol.force.com/support/s/article/DS1000E-Waveform-Data-Formatting-Guide).

Unfortunately they provide the wrong formula for the conversion of an array
index of the waveform data into time for "long" data (i.e., 8192 bytes or
more). Copy&paste from the FAQ:

> To get time in seconds, for each point:
>
>     T(s) = <Time_Offset> -[ (<Points> - 10) / (1 / (2*<Samp_Rate>)]

Firstly, the formula has four openening brackets  `(` but only three
closing brackets `)`.

Then there is a "double division". The expression


    (<Points> - 10) / (1 / (2*<Samp_Rate>))

can be simplified to

    (<Points> - 10) * 2 * <Samp_Rate>

(Missing closing bracket added by the author of this text.)

This does not look right, even formally: The left side of equation
(`T(s)`) is supposed to be a time, specified in seconds; on the right
side we have `<Points>`, which, I assume, means the index of a "data
point" in the byte array. This is a value without a dimension or physical
unit, then we have two constants - also without physical units - and the
sample rate, usually specified in the unit 1/s, or Hz. All these terms
are added/multiplied, so we have an expression that gives us a value in
the unit 1/s, or the inverse of time. And that cannot be set equal to some
time value...

A more sane looking formula would be

    T(s) = <Time_Offset> -[ (<Points> - 10) / (2*<Samp_Rate>)]

But this is not usable either: With <Time_Offset> == 0, we have

    T(s) =  -[ (<Points> - 10) / (2*<Samp_Rate>)]

Let's assume a data acquisition with 8192 data points. Then `<Points>`
would range from 0 to 8191. Rigol's conversion formula would map
these indexes to the values

    T(0) = 10 / (2*<Samp_Rate>)
    T(8191) = -8181 / (2*<Samp_Rate>)

<Samp_Rate> is usually given as a positive number (what would a negative
sampling rate be?), so the first data point (index 0) is mapped to a positive
value, and the last value (index 8191) is mapped to a negative value. This
suggests that the DS1000E/D sends the sampled data "backwards", i.e., the
last value at index 0, and the first value at index 8191. But this is not
the case: The device sends the first sampled value at index 0 and the last
value at index 8191, as one would expect.

Flipping the sign is not enough. This results in the values

    T(0) = -10 / (2*<Samp_Rate>)
    T(8191) = 8181 / (2*<Samp_Rate>)

The factor 2 in the divisor does not make sense either: The term "sample
rate" is usually understood as, well, how many samples are taken per time
unit, hence the time distance between two conscutive samples should
be 1/<Samp_Rate>, not 1 / (2 * <Samp_Rate>).

If the trigger time is supposed to be at time zero, one would expect
something like

    T(0) = -4096 / <Samp_Rate>
    T(4096) = 0
    T(8191) = 4095 / <Samp_Rate>

When the timebase offset is zero, the DS1000E/D displays half of the samples
from the time before the trigger event, and the half of the samples from
the time at and after the trigger event.

So, a formula like

    T(index) = time_offset + (index - num_samples/2) / sample_rate

makes more sense. The number 2 as a divisor looks reasonable in the
calculation – if combined with the right denominator.

One "magic number" remains: 10 is some offset in Rigol's formula.
Some kind of latency could be reasonable. But does this latency indeed
depend on the sample rate?
