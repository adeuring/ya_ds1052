#!/bin/env python
"""Issueing `:WAV:DATA?` shortly after a `:RUN` or `:AUTO` command can
crash the DS1000D/E: The device does no longer respond to any commands,
it must be power-cycled to be again usable.

This script sends one of the commands `:RUN`, `:STOP`, `:AUTO` to a DS1000,
followed by `:WAV:DATA?` after a defined delay; this delay is decreased
until the DS1000 does no longer respond to `:WAV:DATA?` or until the delay
value 0 is reached.
"""
from argparse import ArgumentParser, RawTextHelpFormatter
from ds1052 import (
    ChannelCoupling, DS1052, DS1052TimeoutError, Slope, Sweep, PointsMode,
    TriggerMode,
    )
from pyvisa import ResourceManager
from textwrap import dedent
from time import sleep


def add_enum_arg(p, name, enum, default=None):
    return p.add_argument(
        name, choices=enum, type=enum.__getitem__, default=default)


def get_args():
    p = ArgumentParser(
        description=__doc__, formatter_class=RawTextHelpFormatter)
    p.add_argument('--delay-start', type=float, default=0.15)
    p.add_argument('--delay-step', type=float, default=0.01)
    add_enum_arg(p, '--sweep', Sweep, Sweep.auto)
    add_enum_arg(p, '--points-mode', PointsMode, PointsMode.normal)
    sp = p.add_subparsers(help='command sequence to test')
    stop_run_parser = sp.add_parser('stop_run', help=stop_run.__doc__)
    stop_run_parser.set_defaults(func=stop_run)
    run_stop_parser = sp.add_parser('run_stop', help=run_stop.__doc__)
    run_stop_parser.set_defaults(func=run_stop)
    auto_parser = sp.add_parser('auto', help=auto.__doc__)
    auto_parser.set_defaults(func=auto)
    return p.parse_args()


def delay_iter(args):
    value = args.delay_start
    while True:
        yield value
        if value <= 0:
            break
        value -= args.delay_step


def stop_run(delay, args):
    """Issue `:STOP`, wait for 0.12s, issue `:RUN`, wait for `delay`
    seconds then issue 10 times `:WAV:DATA?`.

    Will crash the DS1052 if `delay` <= 0.05.
    """
    dso._tmc_device.write(':STOP')
    sleep(0.12)
    dso._tmc_device.write(':RUN')
    sleep(delay)
    for read_count in range(10):
        try:
            dso.read_waveforms((1, 2), args.points_mode)
        except DS1052TimeoutError:
            return False
    return True


def run_stop(delay, args):
    """Issue `:RUN`, wait for 0.12s, issue `:STOP`, wait for `delay`
    seconds then issue 20 times `:WAV:DATA?`.

    No crashes noticed so far.
    """
    dso._tmc_device.write(':RUN')
    sleep(0.12)
    dso._tmc_device.write(':STOP')

    sleep(delay)
    for read_count in range(10):
        try:
            dso.read_waveforms((1, 2), args.points_mode)[0].raw_waveform
        except DS1052TimeoutError:
            return False
    return True


def auto(delay, args):
    """ `:AUTO`, wait for `delay` seconds then issue 10 times
    `:WAV:DATA?`.

    `:WAV:DATA?` will crash the DSO whem issued while the device
    is processing the :AUTO command. The execution of this command
    has not a fixed duration: When a "good" signal is present it
    can take 3 seconds; if no signals at all are connected to the
    inputs, the device seems to stay in AUTO mode for a much
    longer time. Maybe until explicitly stopped.
    """
    dso._tmc_device.write(':AUTO')
    sleep(delay)
    for read_count in range(10):
        try:
            dso.read_waveforms((1, 2), args.points_mode)[0].raw_waveform
        except DS1052TimeoutError:
            return False
    return True


def test_loop(test, delay):
    for count in range(20):
        if not test(delay):
            return False
    return True


args = get_args()
print(args)
#rm = ResourceManager('@ivi')
rm = ResourceManager('@py')
with DS1052(resource_manager=rm) as dso:
    dso.trigger[dso.trigger.mode].sweep = args.sweep
    for delay in delay_iter(args):
        print(f'Delay: {delay:4.2f}')
        # "max(delay, 0)": prevent possible exception in sleep() due to
        # float rounding errors, or inconsistent delay iteration parameters.
        if not args.func(max(delay, 0), args):
            break
